package telnet;

import java.io.*;
import java.net.*;

public class TelnetServer {
	
	static BufferedReader brremote;
	static String received;
	
	public static void main(String args[]) {
		String response = "Thanks for sending: ";
		try {
			// Create object of Server Socket
			System.out.print("Creating server socket...");
			ServerSocket srvr = new ServerSocket(3083);
			System.out.println("...server socket created!");
			// Socket object that listens the port (3083) and accepts the
			// incoming connection requests
			System.out.print("Waiting for client...");
			Socket skt = srvr.accept();
			System.out.println("...client connected!");
			
			// Object of Buffered Reader to read command from terminal
			//BufferedReader brlocal = new BufferedReader(new InputStreamReader(System.in));
			brremote = new BufferedReader(new InputStreamReader(skt.getInputStream()));	
			
//			Runnable runnable2 = new BufferThread();
//	        Thread thread2 = new Thread(runnable2);
//	        thread2.start();
	        
			// Create object of Output Stream to write on socket
			DataOutputStream dout = new DataOutputStream(skt.getOutputStream());

			System.out.println("Waiting to receive commands...");
			received = response;
			
//			while (true) {
//				received = din.readUTF();//.readLine(); //
//				System.out.println("Received: " + received);
//				//out.print(response);
//				dout.writeUTF(response + received + "\n");//.write(response + received + "\n"); //
//				//System.out.println("Sent response!");
//				
//				if (received.equals("exit")) {
//					// sends response to incoming request if command is '1'
//					System.out.println("Exiting...");
//					break;
//				}
//			}	
			
//			int c=0;
//			while ((c = brremote.read()) != -1){
//				received += (char)c;
////			    System.out.println(received);		
//			    //textArea.append(String.valueOf((char)c));
//			    //dout.writeBytes(received + "\n\r");//.write(response + received + "\n"); //
//			}
			System.out.println(received);		
			dout.writeBytes(received + "\n\r");//.write(response + received + "\n"); //
			dout.writeInt(0);//.write(response + received + "\n"); //

			dout.close();// close out
			skt.close();// close skt
			srvr.close();// close srvr
			brremote.close(); // close din
		} catch (Exception e) {
			System.out.print(e);
		}
	}
	
	private void receiveInput() throws IOException{ // throws Exception
		int c=0;
		while ((c = brremote.read()) != -1){//-1
			received += (char)c;
		    //System.out.print((char)c);		
//		    textArea.append(String.valueOf((char)c));
		}
	}
	
	class BufferThread implements Runnable {
	    public void run() {
	        try {
				receiveInput();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	}	
}

