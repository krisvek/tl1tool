package telnet;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.AbstractAction;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import javax.swing.SwingConstants;

import java.awt.Component;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.Box;

public class GUI {

	private JFrame frame;
	//private final Action action = new SwingAction();
	private JTextField txtAddress;
	private JTextField txtPort;
	private JTextField txtTid;
	private JTextField txtUser;
	private JTextField txtPassword;
	private JTextArea textArea;
	
	private Socket soc = null;
	private JLabel lblUsername;
	private JLabel lblPassword;
	private JPanel panel;
	private JPanel panel_1;
	private Box verticalBox;
	private JPanel panel_2;
	
	BufferedReader brremote;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() throws Exception{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(true);
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setMinimumSize(new Dimension(300, 400));
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.WEST);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		
		panel_2 = new JPanel();
		panel.add(panel_2);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		verticalBox = Box.createVerticalBox();
		panel_2.add(verticalBox, BorderLayout.NORTH);
		
		JLabel lblIpAddress = new JLabel("IP Address");
		verticalBox.add(lblIpAddress);
		lblIpAddress.setHorizontalAlignment(SwingConstants.CENTER);
		
		txtAddress = new JTextField();
		verticalBox.add(txtAddress);
		txtAddress.setText("localhost");
		txtAddress.setAlignmentY(Component.TOP_ALIGNMENT);
		txtAddress.setColumns(10);
		
		JLabel lblPort = new JLabel("Port");
		verticalBox.add(lblPort);
		lblPort.setHorizontalAlignment(SwingConstants.CENTER);
		
		txtPort = new JTextField();
		verticalBox.add(txtPort);
		txtPort.setText("3083");
		txtPort.setAlignmentY(Component.TOP_ALIGNMENT);
		txtPort.setColumns(10);
		
		JLabel lblTid = new JLabel("TID");
		verticalBox.add(lblTid);
		lblTid.setHorizontalAlignment(SwingConstants.CENTER);
		
		txtTid = new JTextField();
		verticalBox.add(txtTid);
		txtTid.setText("test");
		txtTid.setAlignmentY(Component.TOP_ALIGNMENT);
		txtTid.setColumns(10);
		
		lblUsername = new JLabel("Username");
		verticalBox.add(lblUsername);
		lblUsername.setHorizontalAlignment(SwingConstants.CENTER);
		
		txtUser = new JTextField();
		verticalBox.add(txtUser);
		txtUser.setText("kevin");
		txtUser.setAlignmentY(Component.TOP_ALIGNMENT);
		txtUser.setColumns(10);
		
		lblPassword = new JLabel("Password");
		verticalBox.add(lblPassword);
		lblPassword.setHorizontalAlignment(SwingConstants.CENTER);
		
		txtPassword = new JTextField();
		verticalBox.add(txtPassword);
		txtPassword.setText("secret");
		txtPassword.setAlignmentY(Component.TOP_ALIGNMENT);
		txtPassword.setColumns(10);
		// CENTER will use up all available space
		
		JButton button = new JButton("Initiate Connection");
		panel_2.add(button, BorderLayout.SOUTH);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0){
				Runnable runnable1 = new ConnectionThread();
	            Thread thread1 = new Thread(runnable1);
	            thread1.start();
			}
		});
		
		panel_1 = new JPanel();
		frame.getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		textArea = new JTextArea("No output yet.");
		textArea.setEditable(false);
		//frame.getContentPane().add(textArea);	
		
		JScrollPane scroll = new JScrollPane(textArea);
		panel_1.add(scroll);
	}
	
	private void initiateConnection() throws IOException{ // throws Exception
		
		// Create object of Socket		
		String status1 = "Creating socket to " + txtAddress.getText() + " on port "
				+ txtPort.getText() + "...\r\n";
		System.out.print(status1);
        if (status1.length() > 0)
          textArea.replaceRange(status1, 0, textArea.getText().length());	
        
		try {
			soc = new Socket(txtAddress.getText(), Integer.parseInt(txtPort.getText()));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			textArea.append(e.toString());
			e.printStackTrace();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			textArea.append(e.toString());
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			textArea.append(e.toString());
			e.printStackTrace();
		}
		try {
			soc.setKeepAlive(true);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			textArea.append(e.toString());
			e.printStackTrace();
		}
		
		System.out.println("...socket created!");
		textArea.append("...socket created!");
		
		String command="act-user::" + txtUser.getText() + ":gdt::" + txtPassword.getText() +";";
		
		// Create object of Output Stream to write on socket
		DataOutputStream dout = new DataOutputStream(soc.getOutputStream());
		
		// Object of Buffered Reader to read command from terminal
		BufferedReader brlocal = new BufferedReader(new InputStreamReader(System.in));
		brremote = new BufferedReader(new InputStreamReader(soc.getInputStream()));		
		
		Runnable runnable2 = new BufferThread();
        Thread thread2 = new Thread(runnable2);
        thread2.start();
		
		System.out.println("Welcome to Kevin's Telnet/TL1 Command Executor!");
		textArea.append("\r\nWelcome to Kevin's Telnet/TL1 Command Executor!");		
		
		System.out.println("Sending the following commands: ");
		textArea.append("\r\nSending the following commands: ");
		String commandArray[] = new String[]{
				command,
				"RTRV-NE-GEN:::gdt;",
				"RTRV-ALM-ALL:::gdt;",
				"RTRV-ALM-BITS:::gdt;",
				"RTRV-ALM-ENV:::gdt;",
				"RTRV-ALM-SYNCN:::gdt;",
				"RTRV-COND-ALL:::gdt;",
				"RTRV-COND-ENV:::gdt;",
				"RTRV-COND-BITS:::gdt;",
				"RTRV-COND-SYNCN:::gdt;",
				"RTRV-INV::ALL:gdt;",
				"RTRV-NE-APC::ALL:gdt;",
				"CANC-USER::CISCO15:gdt;"
		};		
		for (int i = 0; i < commandArray.length; i++){
			dout.writeBytes(commandArray[i]);
			System.out.println("\r\n" + commandArray[i]);
			textArea.append("\r\n" + commandArray[i]);			
		}
		dout.writeInt(0);
		
//		int c=0;
//		while ((c = brremote.read()) != 0){//-1
//		    System.out.print((char)c);		
//		    textArea.append(String.valueOf((char)c));
//		}
		
		soc.close(); // close port
//		din.close(); // close input stream
		dout.close(); // close output stream
		brremote.close(); // close buffered Reader
		brlocal.close(); // close buffered Reader
	}
	
	private void receiveInput() throws IOException{ // throws Exception
		int c=0;
		while ((c = brremote.read()) != -1){//-1
		    System.out.print((char)c);		
		    textArea.append(String.valueOf((char)c));
		}
	}
	
	class ConnectionThread implements Runnable {
	    public void run() {
	        try {
				initiateConnection();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	}
	
	class BufferThread implements Runnable {
	    public void run() {
	        try {
				receiveInput();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	}

//	@SuppressWarnings({ "unused", "serial" })
//	private class SwingAction extends AbstractAction {
//		public SwingAction() {
//			putValue(NAME, "Initiate Connection");
//			putValue(SHORT_DESCRIPTION, "YESSSSSSSSSSSSSSSSS");
//		}
//		public void actionPerformed(ActionEvent e) {
//		}
//	}
}
